cmake_minimum_required(VERSION 3.10)

project(
	Visualization
	VERSION 0.1
	DESCRIPTION "Библиотека для построения различных визуализаций исходного кода на языке C"
	LANGUAGES CXX
)

set(CMAKE_CXX_STANDART 14)
set(CMAKE_CXX_STANDART_REQUIRED ON)
if (CMAKE_BUILD_TYPE STREQUAL "Debug")
	set(CMAKE_CXX_FLAGS "-frtti -std=c++14 -g -fstandalone-debug ${ADDITIONAL_CXX_FLAGS}")
else()
	set(CMAKE_CXX_FLAGS "-frtti -std=c++14 ${ADDITIONAL_CXX_FLAGS}")
endif()

SET(CLANG_LIBNAMES 
  	clangTooling
  	clangFrontendTool
  	clangFrontend
  	clangDriver
  	clangSerialization
  	clangToolingSyntax
  	clangRewriteFrontend
	clangAST
  	clangBasic
  	clangStaticAnalyzerFrontend
	clang
)

if(EXISTS ${PATH_TO_LLVM}/llvm/CMakeLists.txt)
	set(LLVM_ENABLE_PROJECTS
		clang
		clang-tools-extra
	)
	set(LLVM_TARGET_TO_BUILD X86)
	add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/${PATH_TO_LLVM}/llvm" llvm-build-dir)

 	if (NOT TARGET clangTooling)
    		message(FATAL_ERROR "Cannot find clangTooling target. Did you forget to clone clang sources? Clean CMake cache and make sure they are available at: ${PATH_TO_LLVM}/tools/clang")
  	endif()

	get_target_property(LLVM_INCLUDE_DIRS LLVMSupport INCLUDE_DIRECTORIES)
	foreach(clang_libname ${CLANG_LIBNAMES})
  		get_target_property(clang_target_includes ${clang_libname} INCLUDE_DIRECTORIES)
		set(LLVM_INCLUDE_DIRS ${LLVM_INCLUDE_DIRS} ${clang_target_includes})
	endforeach()

	list(REMOVE_DUPLICATES LLVM_INCLUDE_DIRS)
else()
	set (search_paths
  		${PATH_TO_LLVM}
  		${PATH_TO_LLVM}/lib/cmake
  		${PATH_TO_LLVM}/lib/cmake/llvm
  		${PATH_TO_LLVM}/lib/cmake/clang
  		${PATH_TO_LLVM}/share/clang/cmake/
  		${PATH_TO_LLVM}/share/llvm/cmake/
	)

	find_package(LLVM REQUIRED CONFIG
		PATHS ${search_paths}
		NO_DEFAULT_PATH
	)
	message(STATUS "Path to LLVM ${PATH_TO_LLVM}")
	message(STATUS "Found LLVM ${LLVM_PACKAGE_VERSION} in ${LLVM_INSTALL_PREFIX}")
	message(STATUS "Using LLVMConfig.cmake in ${LLVM_DIR}")

	find_package(Clang REQUIRED CONFIG
		PATHS ${search_paths}
		NO_DEFAULT_PATH
	)
	message(STATUS "Found Clang in ${CLANG_INSTALL_PREFIX}")
	message(STATUS "Found Cland headers in ${CLANG_INCLUDE_DIRS}")
	message(STATUS "Found LLVM headers in ${LLVM_INCLUDE_DIRS}")
endif()

string(TOLOWER ${PROJECT_NAME} DEFAULT_OUTPUT_NAME)
string(CONCAT EXE_TARGET_NAME ${DEFAULT_OUTPUT_NAME} "_exe")
string(CONCAT BINDINGS_TARGET_NAME ${DEFAULT_OUTPUT_NAME} "_py_bindings")

add_executable(${EXE_TARGET_NAME} 
	$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src/main.cpp>
	$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src/to_dot.cpp>
	$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src/build_cfg.cpp>
)

target_include_directories(${EXE_TARGET_NAME} PRIVATE
	$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include/visualization>
)

target_include_directories(${EXE_TARGET_NAME} PUBLIC ${LLVM_INCLUDE_DIRS})
target_include_directories(${EXE_TARGET_NAME} PUBLIC ${CLANG_INCLUDE_DIRS})
target_link_libraries(${EXE_TARGET_NAME} PRIVATE ${CLANG_LIBNAMES})
target_compile_definitions(${EXE_TARGET_NAME} PUBLIC ${LLVM_DEFINITIONS})
target_compile_definitions(${EXE_TARGET_NAME} PUBLIC ${Clang_DEFINITIONS})
set_target_properties(${EXE_TARGET_NAME} PROPERTIES 
	OUTPUT_NAME ${DEFAULT_OUTPUT_NAME}
)

add_subdirectory(${PATH_TO_PYBIND11})
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/src/python_bindings)
