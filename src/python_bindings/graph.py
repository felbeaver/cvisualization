from visualization import _graph
from visualization import CLANG_HEADERS_PATH

def cfg_for_code(code, filename, funcs=None):
    """Возвращает граф потока управления (CFG) для функций исходного 
    кода, который храниться в памяти. Предпологается, что это строка 
    исходного кода одного файла.

    Возвращает граф потока управления (CFG) в DOT формате для функций, 
    названия который указываются в параметре *funcs*. Если *funcs* = 
    None (по-умолчанию), то графы строятся для каждой функции файла.

    Параметр *filename* используется для того, чтобы привязать код из памяти к имени файла.

    :param string code: строка с исходным кодом
    :param string filename: имя файла
    :param list funcs: список названий функций
    :returns: словарь вида (название_функции:dot_представление_графа) (*string*:*string*)
    :rtype: dict
    """
    if funcs:
        cfgs = _graph.build_dot_cfg_for_funcs(
                code, filename, funcs, CLANG_HEADERS_PATH)
    else:
        cfgs = _graph.build_dot_cfg_for_any_func(
                code, filename, CLANG_HEADERS_PATH)

    return cfgs

