"""Привязки C++ библиотеки visualization

Пакет предоставляет доступ к функциям C++ библиотеки visualization.

:Доступные модули:
   * graph
"""
import os

CLANG_HEADERS_PATH = os.path.abspath('clang_headers')
__all__ = ['graph']

