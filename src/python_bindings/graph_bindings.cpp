#include "to_dot.h"
#include "build_cfg.h"
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <vector>
#include <string>

const std::map<std::string, std::string> 
build_dot_cfg_for_funcs(const std::string& code, 
		const std::string& filename, 
		const std::vector<std::string>& func_names,
		const std::string& headers_path)
{
    std::map<std::string, std::string> dot_cfgs;
    auto cfgs = build_cfg(code, filename, headers_path, func_names);

    for (const auto& [key, value] : cfgs) {
        dot_cfgs[key] = to_dot(*value, key);
    }

    return dot_cfgs;
}

const std::map<std::string, std::string> 
build_dot_cfg_for_any_func(const std::string& code, 
		const std::string& filename, 
		const std::string& headers_path)
{
    std::map<std::string, std::string> dot_cfgs;
    auto cfgs = build_cfg(code, filename, headers_path);

    for (const auto& [key, value] : cfgs) {
        dot_cfgs[key] = to_dot(*value, key);
    }

    return dot_cfgs;
}

PYBIND11_MODULE(_graph, m) 
{
    m.def("build_dot_cfg_for_funcs", &build_dot_cfg_for_funcs);
    m.def("build_dot_cfg_for_any_func", &build_dot_cfg_for_any_func);
}
