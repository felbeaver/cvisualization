#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchersInternal.h"
#include "llvm/ADT/StringRef.h"
#include "clang/Tooling/Tooling.h"
#include "clang/Frontend/ASTUnit.h"
#include "clang/Serialization/PCHContainerOperations.h"
#include "clang/Basic/LangOptions.h"
#include "llvm/Support/GraphWriter.h"
#include "clang/Analysis/CFG.h"
#include "llvm/Support/raw_ostream.h"
#include <string>
#include <vector>
#include <memory>
#include <streambuf>
#include <sstream>
#include <algorithm>
#include <map>
#include "build_cfg.h"

using std::string;
using std::vector;
using namespace clang::tooling;
using namespace clang;
using namespace clang::ast_matchers;

namespace {

class CFGBuilder : public MatchFinder::MatchCallback {
public:
    CFGBuilder(const std::string& matcher_bind) : 
        matcher_bind(matcher_bind)
    {}

    virtual void run(const MatchFinder::MatchResult &result) 
    {
        if (const FunctionDecl *func_decl = 
            result.Nodes.getNodeAs<clang::FunctionDecl>(
                this->matcher_bind)
            ) {
            ASTContext *context = result.Context;
            Stmt *func_body = func_decl->getBody();
            std::unique_ptr<CFG> source_cfg = CFG::buildCFG(
                    func_decl, func_body, context, 
                    clang::CFG::BuildOptions());
            //auto lang_opt = context->getLangOpts();
            //source_cfg->dump(langOpt, true);
            this->cfgs[func_decl->getName().str()] = 
                std::move(source_cfg);
        }
    }

    const std::map<std::string, std::shared_ptr<CFG>> get_cfgs()
    {
        return std::move(this->cfgs);
    }
private:
    const std::string matcher_bind;
    std::map<std::string, std::shared_ptr<CFG>> cfgs;
};

template<typename T> T* get_pointer(T& t) { return &t; }
llvm::StringRef get_str_ref(const std::string& s) 
{ 
    return llvm::StringRef(s);
}

}

const std::map<std::string, std::shared_ptr<CFG>> build_cfg(
        const std::string& code, const std::string& filename, 
        const std::string& headers_path, 
        const DeclarationMatcher& func_matcher, 
        const std::string& bind_name)
{ 
    std::vector<std::string> args{ "-I" + headers_path, 
	    "-fsyntax-only", "--std=c99" };
    std::unique_ptr<ASTUnit> unit = 
        buildASTFromCodeWithArgs(code, args, filename);
    
    CFGBuilder builder(bind_name);
    MatchFinder finder;
    finder.addMatcher(func_matcher, &builder);
    finder.matchAST(unit->getASTContext());

    return builder.get_cfgs();
}

const std::map<std::string, std::shared_ptr<CFG>> build_cfg(
        const std::string& code, const std::string& filename, 
        const std::string& headers_path)
{
    DeclarationMatcher func_matcher = 
        functionDecl(isExpansionInMainFile()).bind("anyFunction");
    return build_cfg(code, filename, headers_path, func_matcher, 
              "anyFunction");
}

const std::map<std::string, std::shared_ptr<CFG>> build_cfg(
        const std::string& code, const std::string& filename, 
        const std::string& headers_path, 
        const std::vector<std::string>& func_names)
{
    std::vector<llvm::StringRef> str_refs(func_names.size());
    std::transform(
            func_names.begin(), func_names.end(), str_refs.begin(), 
            get_str_ref);

    DeclarationMatcher func_matcher = 
        functionDecl(allOf(isExpansionInMainFile(), 
                    hasAnyName(str_refs))
                ).bind("selectedFunction");
    return build_cfg(code, filename, headers_path, func_matcher, 
              "selectedFunction");
}
