#include "build_cfg.h"
#include "to_dot.h"
#include "clang/Basic/LangOptions.h"
#include <filesystem>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <string>

int main(int argc, char *argv[])
{
    const std::string filename = argv[1];
    std::ifstream ifs(filename);

    if (ifs) {
        std::string code((std::istreambuf_iterator<char>(ifs)),
                std::istreambuf_iterator<char>());
        std::cout << "Read code from file: " << std::endl
            << code << std::endl;
        const std::filesystem::path rel_path = 
            "../lib/clang+llvm-11.0.1-x86_64-linux"
             "-gnu-ubuntu-20.10/lib/clang/11.0.1/include/";
        //std::vector<std::string> func_names{ "main" };
        auto cfgs = build_cfg(code, "struct_test.c", 
              std::filesystem::absolute(rel_path));//, func_names);
        //cfgs["main"]->dump(LangOptions(), true);
        //std::cout << to_dot(*cfgs["main"], "main") << std::endl;
        
        for  (const auto& [key, value] : cfgs) {
            std::cout << "Function " << key << ": " << std::endl;
            //std::cout << to_dot(*value, key) << std::endl;
	    //value->dump(clang::LangOptions(), true);
	    print_cfg(&(*value), clang::LangOptions(), true);
        }
    } else {
        perror(filename.c_str());
    }

    return 0;
}
