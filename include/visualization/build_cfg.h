#ifndef VISUALIZATION_BUILD_CFG
#define VISUALIZATION_BUILD_CFG

#include <map>
#include <string>
#include <memory>
#include <vector>
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/Analysis/CFG.h"

const std::map<std::string, std::shared_ptr<clang::CFG>> build_cfg(
        const std::string& code, const std::string& filename, 
        const std::string& headers_path, 
        const clang::ast_matchers::DeclarationMatcher& func_matcher, 
        const std::string& bind_name);

const std::map<std::string, std::shared_ptr<clang::CFG>> build_cfg(
        const std::string& code, const std::string& filename, 
        const std::string& headers_path);

const std::map<std::string, std::shared_ptr<clang::CFG>> build_cfg(
        const std::string& code, const std::string& filename, 
        const std::string& headers_path, 
        const std::vector<std::string>& func_names);

#endif
