#ifndef VISUALIZATION_TO_DOT
#define VISUALIZATION_TO_DOT

#include "clang/Analysis/CFG.h"

std::string to_dot(const clang::CFG& cfg, const std::string& title);
std::string print_cfg(clang::CFG* cfg, 
		const clang::LangOptions &LO, bool ShowColors);

#endif
